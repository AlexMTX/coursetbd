package service;

import model.*;

import java.util.*;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


@Path("/service")
public class CourseService {
	
	private static ApplicationContext ctx = new GenericXmlApplicationContext("MongoConfig.xml");
	private static MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
	
	@POST
	@Path("/storage")
	@Consumes(MediaType.APPLICATION_XML)
	public void recieveForm(Form form) {
		List<String> list = form.getIntersection().getStreetList();
		Query streetListSearchQuery = new Query(Criteria.where("_id").is("53725406818a24372fd23d73"));
		StreetList streetList = mongoOperation.findOne(streetListSearchQuery, StreetList.class);
		streetList.append(list);
		mongoOperation.save(streetList);
		
		List<String> streetListBufer = form.getIntersection().getStreetList();
		for (String str: streetListBufer) {
			Query query = new Query(Criteria.where("_class").is("model.RelatedStreetList").and("name").is(str));
			RelatedStreetList relatedStreetList;
			if (mongoOperation.exists(query, RelatedStreetList.class)) {
				relatedStreetList = mongoOperation.findOne(query, RelatedStreetList.class);
			} else {
				relatedStreetList = new RelatedStreetList();
			}
			List<String> listBufer = streetListBufer;
			listBufer.remove(str);
			relatedStreetList.append(listBufer);
			mongoOperation.save(relatedStreetList);			
		}
		
		
		for (String str: streetListBufer) {
			for(int i=(streetListBufer.indexOf(str)+1); i<streetListBufer.size(); i++) {
				FormListWrapper formListWrapper = new FormListWrapper();
				formListWrapper.setStreet(str);
				formListWrapper.setSecondStreet(streetListBufer.get(i));
				formListWrapper.getFormList().add(form);
				Query query = new Query(Criteria.where("_class").is("model.FormListWrapper")
						.and("street").is(str)
						.and("secondStreet").is(streetListBufer.get(i)));
				FormListWrapper formListBufer;
				if (mongoOperation.exists(query, FormListWrapper.class)) {
					formListBufer = mongoOperation.findOne(query, FormListWrapper.class);
				} else {
					formListBufer = new FormListWrapper();
				}
				
				formListBufer.getFormList().add(form);
				
			}
		}
		
	}
	
	@GET
	@Path("/analitics")
	@Produces(MediaType.APPLICATION_XML)
	public StreetList getStreetList() {
		Query streetListSearchQuery = new Query(Criteria.where("_id").is("53725406818a24372fd23d73"));
		StreetList streetList = mongoOperation.findOne(streetListSearchQuery, StreetList.class);
		return streetList;
	}
	
	@GET
	@Path("/analitics")
	@Produces(MediaType.APPLICATION_XML)
	public StreetList getRelatedStreetList(@QueryParam("street") String street) {
		Query query = new Query(Criteria.where("_class").is("model.RelatedStreetList").and("name").is(street));
		RelatedStreetList relatedStreetList = mongoOperation.findOne(query, RelatedStreetList.class);
		StreetList streetList = new StreetList();
		streetList.append(relatedStreetList.getStreet());		 
		return streetList;
	}
	
	@GET
	@Path("/analitics")
	@Produces(MediaType.APPLICATION_XML)
	public List<Form> getFormList(@QueryParam("street") String street,
									@QueryParam("secondStreet") String secondStreet) {
		return new ArrayList<Form>();
	}
	
	@GET
	@Path("/analitics")
	@Produces(MediaType.APPLICATION_XML)
	public Form getForm(@QueryParam("id") String ID) {
		Query query = new Query(Criteria.where("_id").is(ID));
		Form form = mongoOperation.findOne(query, Form.class);
		return form;
	}

}
