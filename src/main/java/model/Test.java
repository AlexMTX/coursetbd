package model;

import java.io.*;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;

public class Test {

	public static void main(String[] args) throws DatatypeConfigurationException { 
		ObjectFactory of = new ObjectFactory();
		Form form = of.createForm();
		Date date = new Date();
		form.setDate(date);
		form.setObserver("Chuck");
		Date time = new Date();
		form.setStartTime(time);
		form.setPeriod(15);
		Intersection intersection = of.createIntersection();
		intersection.setLatitude(22.2342);
		intersection.setLongitude(64.657);
		intersection.getStreetList().add("������� ��������");
		intersection.getStreetList().add("��������� ��������");
		form.setIntersection(intersection);
		Location location = of.createLocation();
		location.setCountry("�����");
		location.setRegion("�����-���������");
		form.setLocation(location);
		Direction origin = of.createDirection();
		origin.setID(1);
		origin.setStreet("������� ��������");
		form.setOrigin(origin);
		Destination temp = of.createDestination();
		Direction tempDirection = of.createDirection();
		tempDirection.setID(2);
		tempDirection.setStreet("��������� ��������");
		temp.setDirection(tempDirection);
		Measurement tempMeasurement = of.createMeasurement();
		tempMeasurement.setMode("�������� ����������");
		tempMeasurement.setModeRate(1);
		tempMeasurement.setCount(37);
		temp.getMeasurementList().add(tempMeasurement);
		form.getDestination().add(temp);
		
		try {
			File file = new File("test.xml");
			
			JAXBContext jaxbContext = JAXBContext.newInstance(Form.class);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(form, file);
			
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			Form testForm = (Form) jaxbUnmarshaller.unmarshal(file);
			System.out.println(testForm);
			System.out.println(testForm.getDate());
			System.out.println(testForm.getObserver());
			
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		

	}

}
