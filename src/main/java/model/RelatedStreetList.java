package model;

import java.util.*;

public class RelatedStreetList {

	private String name;
    private List<String> street;

    public List<String> getStreet() {
        if (street == null) {
            street = new ArrayList<String>();
        }
        return this.street;
    }

    public void append(List<String> list) {
    	if (street == null) {
            street = new ArrayList<String>();
        }
    	for(String str: list) {
    		if(!this.street.contains(str)) this.street.add(str);
    	}
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
