package model;

public class FormListWrapper {
	
	private FormList formList;
	private String street;
	private String secondStreet;
	
	public FormList getFormList() {
		return formList;
	}
	
	public void setFormList(FormList formList) {
		this.formList = formList;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getSecondStreet() {
		return secondStreet;
	}

	public void setSecondStreet(String secondStreet) {
		this.secondStreet = secondStreet;
	}

	

}
