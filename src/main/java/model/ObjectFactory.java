package model;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public Form createForm() {
        return new Form();
    }

    public Intersection createIntersection() {
        return new Intersection();
    }

    public Location createLocation() {
        return new Location();
    }

    public Direction createDirection() {
        return new Direction();
    }

    public Destination createDestination() {
        return new Destination();
    }

    public Measurement createMeasurement() {
        return new Measurement();
    }
    
    public StreetList createStreetList() {
        return new StreetList();
    }
    
    public Form createFormListForm() {
        return new Form();
    }

    public FormList createFormList() {
        return new FormList();
    }

}
